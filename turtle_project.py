import turtle
import random

color = ["white", "blue", "red", "pink", "yellow", "black"]

sc = turtle.Screen()
sc.title("Pong Game")

sc.bgcolor("white")

sc.setup(width=1000, height=600)

leftpadl = turtle.Turtle()
leftpadl.speed(0)
leftpadl.shape("turtle")
leftpadl.color("black")
leftpadl.shapesize(stretch_wid=4, stretch_len=.5)
leftpadl.penup()
leftpadl.goto(-400, 0)

rightpadl = turtle.Turtle()
rightpadl.speed(0)
rightpadl.shape("circle")
rightpadl.color("black")
rightpadl.shapesize(stretch_wid=4, stretch_len=.5)
rightpadl.penup()
rightpadl.goto(400, 0)

pong_ball = turtle.Turtle()
pong_ball.speed(40)
pong_ball.shape("circle")
pong_ball.color("blue")
pong_ball.penup()
pong_ball.goto(0, 0)
pong_ball.dx = 5
pong_ball.dy = -5

player1 = 0
player2 = 0

score = turtle.Turtle()
score.speed(0)
score.color("pink")
score.penup()
score.hideturtle()
score.goto(0, 260)
score.write("player1 : 0 player2 : 0", align="center", font=("Comic Sans", 24, "normal"))


def padl1up():
    y = leftpadl.ycor()
    y += 20
    leftpadl.sety(y)


def padl1down():
    y = leftpadl.ycor()
    y -= 20
    leftpadl.sety(y)


def padl2up():
    y = rightpadl.ycor()
    y += 20
    rightpadl.sety(y)


def padl2down():
    y = rightpadl.ycor()
    y -= 20
    rightpadl.sety(y)


sc.listen()
sc.onkeypress(padl1up, "w")
sc.onkeypress(padl1down, "s")
sc.onkeypress(padl2up, "Up")
sc.onkeypress(padl2down, "Down")

while True:
    sc.update()

    pong_ball.setx(pong_ball.xcor() + pong_ball.dx)
    pong_ball.sety(pong_ball.ycor() + pong_ball.dy)

    if pong_ball.ycor() > 280:
        pong_ball.sety(280)
        pong_ball.dy *= -1

    if pong_ball.ycor() < -280:
        pong_ball.sety(-280)
        pong_ball.dy *= -1

    if pong_ball.xcor() > 500:
        pong_ball.goto(0, 0)
        pong_ball.dy *= -1
        player1 += 1
        score.clear()
        score.write("Player1 : {} Player2: {}".format(player1, player2), align="center", font=("Comic Sans", 24, "normal"))
        sc.bgcolor(random.choice(color))

    if pong_ball.xcor() < -500:
        pong_ball.goto(0, 0)
        pong_ball.dy *= -1
        player2 += 1
        score.clear()
        score.write("Player1 : {} Player2: {}".format(player1, player2), align="center", font=("Comic Sans", 24, "normal"))
        sc.bgcolor(random.choice(color))

    if (pong_ball.xcor() > 360 and pong_ball.xcor() < 370) and (pong_ball.ycor() < rightpadl.ycor() + 40 and pong_ball.ycor() > rightpadl.ycor() - 40):
        pong_ball.setx(360)
        pong_ball.dx *= -1

    if (pong_ball.xcor() < -360 and pong_ball.xcor() > -370) and (pong_ball.ycor() < leftpadl.ycor() + 40 and pong_ball.ycor() > leftpadl.ycor() - 40):
        pong_ball.setx(-360)
        pong_ball.dx *= -1

    if player1 == 10 or player2 == 10:
        turtle.penup()
        turtle.left(90)
        turtle.fd(200)
        turtle.pendown()
        turtle.right(90)

        # flower base
        turtle.fillcolor("red")
        turtle.begin_fill()
        turtle.circle(10, 180)
        turtle.circle(25, 110)
        turtle.left(50)
        turtle.circle(60, 45)
        turtle.circle(20, 170)
        turtle.right(24)
        turtle.fd(30)
        turtle.left(10)
        turtle.circle(30, 110)
        turtle.fd(20)
        turtle.left(40)
        turtle.circle(90, 70)
        turtle.circle(30, 150)
        turtle.right(30)
        turtle.fd(15)
        turtle.circle(80, 90)
        turtle.left(15)
        turtle.fd(45)
        turtle.right(165)
        turtle.fd(20)
        turtle.left(155)
        turtle.circle(150, 80)
        turtle.left(50)
        turtle.circle(150, 90)
        turtle.end_fill()

        # Petal 1
        turtle.left(150)
        turtle.circle(-90, 70)
        turtle.left(20)
        turtle.circle(75, 105)
        turtle.setheading(60)
        turtle.circle(80, 98)
        turtle.circle(-90, 40)

        # Petal 2
        turtle.left(180)
        turtle.circle(90, 40)
        turtle.circle(-80, 98)
        turtle.setheading(-83)

        # Leaves 1
        turtle.fd(30)
        turtle.left(90)
        turtle.fd(25)
        turtle.left(45)
        turtle.fillcolor("green")
        turtle.begin_fill()
        turtle.circle(-80, 90)
        turtle.right(90)
        turtle.circle(-80, 90)
        turtle.end_fill()
        turtle.right(135)
        turtle.fd(60)
        turtle.left(180)
        turtle.fd(85)
        turtle.left(90)
        turtle.fd(80)

        # Leaves 2
        turtle.right(90)
        turtle.right(45)
        turtle.fillcolor("green")
        turtle.begin_fill()
        turtle.circle(80, 90)
        turtle.left(90)
        turtle.circle(80, 90)
        turtle.end_fill()
        turtle.left(135)
        turtle.fd(60)
        turtle.left(180)
        turtle.fd(60)
        turtle.right(90)
        turtle.circle(200, 60)

        tur = turtle.Turtle()
        tur.speed(20)
        tur.color("black", "orange")
        tur.begin_fill()

        for i in range(50):
            tur.forward(300)
            tur.left(170)

        tur.end_fill()
        window = turtle.Screen()
        window.bgcolor('black')
        window.title("Galactic Flower made for Follow Tutorials")

        galatic = turtle.Turtle()
        galatic.speed(20)
        galatic.color('white')

        rotate = int(180)


        def Circles(t, size):
            for i in range(10):
                t.circle(size)
                size = size - 4


        def drawCircles(t, size, repeat):
            for i in range(repeat):
                Circles(t, size)
                t.right(360 / repeat)


        drawCircles(galatic, 150, 10)

        t1 = turtle.Turtle()
        t1.speed(20)
        t1.color('yellow')

        rotate = int(90)


        def Circles(t, size):
            for i in range(4):
                t.circle(size)
                size = size - 10


        def drawCircles(t, size, repeat):
            for i in range(repeat):
                Circles(t, size)
                t.right(360 / repeat)


        drawCircles(t1, 130, 10)
        t2 = turtle.Turtle()
        t2.speed(20)
        t2.color('blue')

        rotate = int(80)


        def Circles(t, size):
            for i in range(4):
                t.circle(size)
                size = size - 5


        def drawCircles(t, size, repeat):
            for i in range(repeat):
                Circles(t, size)
                t.right(360 / repeat)


        drawCircles(t2, 110, 10)
        t3 = turtle.Turtle()
        t3.speed(20)
        t3.color('red')

        rotate = int(90)


        def Circles(t, size):
            for i in range(4):
                t.circle(size)
                size = size - 19


        def drawCircles(t, size, repeat):
            for i in range(repeat):
                Circles(t, size)
                t.right(360 / repeat)


        drawCircles(t3, 80, 10)
        t4 = turtle.Turtle()
        t4.speed(20)
        t4.color('green')

        rotate = int(90)


        def Circles(t, size):
            for i in range(4):
                t.circle(size)
                size = size - 20


        def drawCircles(t, size, repeat):
            for i in range(repeat):
                Circles(t, size)
                t.right(360 / repeat)


        drawCircles(t4, 40, 10)

        turtle.done()
