Python 3.10.8 (v3.10.8:aaaf517424, Oct 11 2022, 10:14:40) [Clang 13.0.0 (clang-1300.0.29.30)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.
>>> import turtle
>>> turtle.speed(8)
>>> turtle.color("red")
>>> 
>>> for i in range(100):
...     turtle.circle(5*i)
...     turtle.circle(-5*i)
...     turtle.left(i)
... 
